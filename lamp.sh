# Local Variables
DBUSER=vagrant
DBPASSWD=vagrant

echo -e "\n--- Okay, installing Packages now... ---\n"

echo -e "\n--- Updating packages list ---\n"
apt-get -qq update

echo -e "\n--- Install base packages ---\n"
apt-get -y install vim curl build-essential python-software-properties git 

#
# MySQL
#

# MySQL setup for development purposes ONLY
echo -e "\n--- Install MySQL specific packages and settings ---\n"
debconf-set-selections <<< "mysql-server mysql-server/root_password password $DBPASSWD"
debconf-set-selections <<< "mysql-server mysql-server/root_password_again password $DBPASSWD"

# Install MySQL
apt-get -y install mysql-server libapache2-mod-auth-mysql php5-mysql 

# Setup Additional MySQL Data (connect remotely, users, databases)
echo -e "\n--- Setting up our MySQL user and db ---\n"

# Create a root user that can connect from anywhere, allow binding from all addresses in config file
sed -i "s/bind-address/#bind-address/g" /etc/mysql/my.cnf  
mysql -uroot -p$DBPASSWD -e "CREATE USER '$DBUSER'@'%' IDENTIFIED BY '$DBPASSWD';" 
mysql -uroot -p$DBPASSWD -e "GRANT ALL PRIVILEGES ON *.* TO '$DBUSER'@'%' WITH GRANT OPTION" 
service mysql restart  


#
# Apache/Php5
#

# Install Apache
echo -e "\n--- Installing Apache packages ---\n"
apt-get -y install apache2  


#
# PHP 5
#
echo -e "\n--- Installing PHP w/Apache and other packages ---\n"
apt-get -y install php5 libapache2-mod-php5 php5-curl php5-gd php5-mcrypt 


#
# Restart Apache after PHP 5 settings have been updated
#
echo -e "\n--- Restarting Apache ---\n"
service apache2 restart 


#
# GoldLine Specific Setup
#

# Add users to MySQL
echo -e "\n--- Add Users/Passwords to the database ---\n"
# With Password Hash
mysql -uroot -p$DBPASSWD -e "GRANT ALL PRIVILEGES ON *.* TO 'TODO'@'TODO'     IDENTIFIED BY PASSWORD '*TODODE4D5E0148820D150E1AA55C060FC91F6713' WITH GRANT OPTION;"
# With Password
mysql -uroot -p$DBPASSWD -e "GRANT ALL PRIVILEGES ON *.* TO 'TODO'@'TODO'    IDENTIFIED BY 'TODO' WITH GRANT OPTION;"

 
echo -e "--- MySQL root user/password: $DBUSER/$DBPASSWD ---\n"

