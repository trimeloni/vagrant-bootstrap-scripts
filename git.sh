#
# Update to get the latest git version instead of the Trusty Package version
#
# Reference: http://askubuntu.com/questions/568591/how-do-i-install-the-latest-version-of-git-with-apt/568596
#
add-apt-repository ppa:git-core/ppa
apt-get update
apt-get install -y git

