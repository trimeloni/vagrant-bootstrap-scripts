# Local Variables
DBUSER=sample-root
DBPASSWD=sample-password

#
# MySQL
#

# MySQL setup for development purposes ONLY
echo -e "\n--- Install MySQL specific packages and settings ---\n"
debconf-set-selections <<< "mysql-server mysql-server/root_password password $DBPASSWD"
debconf-set-selections <<< "mysql-server mysql-server/root_password_again password $DBPASSWD"

# Install MySQL
apt-get -y install mysql-server libapache2-mod-auth-mysql 

# Setup Additional MySQL Data (connect remotely, users, databases)
echo -e "\n--- Setting up our MySQL user and db ---\n"

# Create a root user that can connect from anywhere, allow binding from all addresses in config file
sed -i "s/bind-address/#bind-address/g" /etc/mysql/my.cnf  
mysql -uroot -p$DBPASSWD -e "CREATE USER '$DBUSER'@'%' IDENTIFIED BY '$DBPASSWD';" 
mysql -uroot -p$DBPASSWD -e "GRANT ALL PRIVILEGES ON *.* TO '$DBUSER'@'%' WITH GRANT OPTION" 
service mysql restart  


# Add Other Users to MySQL
#echo -e "\n--- Add Users/Passwords to the database - With Password Hash ---\n"
#mysql -uroot -p$DBPASSWD -e "GRANT ALL PRIVILEGES ON *.* TO 'TODO-USER'@'TODO-HOST' IDENTIFIED BY PASSWORD '*842YD5871DEF75347AEC090EBEA353CECD851B79' WITH GRANT OPTION;"

#echo -e "\n--- Add Users/Passwords to the database - With Password ---\n"
#mysql -uroot -p$DBPASSWD -e "GRANT ALL PRIVILEGES ON *.* TO 'TODO-USER'@'TODO-HOST' IDENTIFIED BY 'TODO-PASSWORD' WITH GRANT OPTION;"

 




echo -e "\n--- Everything is done, here are some things to know: ---\n"
echo -e "--- MySQL root user/password: $DBUSER/$DBPASSWD ---\n"

