#
# Install Terraform
#

#
mkdir /home/vagrant/terraform
cd /home/vagrant/terraform

# Download Terraform. URI: https://www.terraform.io/downloads.html
curl -O https://releases.hashicorp.com/terraform/0.8.2/terraform_0.8.2_linux_amd64.zip

# Unzip and install
unzip terraform_0.8.2_linux_amd64.zip

# Update 
echo '
# Terraform Paths.
export PATH=/home/vagrant/terraform:$PATH
' >>/home/vagrant/.bash_profile

source /home/vagrant/.bash_profile