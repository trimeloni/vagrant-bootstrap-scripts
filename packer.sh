#
# Install Packer
#

mkdir /home/vagrant/packer
cd /home/vagrant/packer

# Download Packer. URI: https://www.packer.io/downloads.html
curl -O https://releases.hashicorp.com/packer/0.12.1/packer_0.12.1_linux_amd64.zip
# Unzip and install
unzip packer_0.12.1_linux_amd64.zip

echo '
# Packer Paths.
export PATH=/home/vagrant/packer/:$PATH
' >>/home/vagrant/.bash_profile

source /home/vagrant/.bash_profile
