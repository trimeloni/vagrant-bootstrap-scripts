#
# Install Node.sj
#
curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
apt-get -y update
apt-get install -y nodejs

# Install node plugin "forever" which allows an app to run non-stop
npm install -g forever
#forever start app.js