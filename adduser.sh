#
# Add a user/password
#  (adds: app/password)
#
useradd app -s /bin/bash -m
usermod -a -G sudo app
chpasswd << 'END'
app:vagrant
END